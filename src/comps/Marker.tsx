import { Marker } from "react-map-gl/maplibre";
import { useRef, useMemo } from "react";
import maplibregl from "maplibre-gl";

const markerImage =
  "https://www.touristisrael.com/wp-content/uploads/2011/02/jerusalem-purple-1024x683.jpg";

interface Marker extends maplibregl.Marker {}

const MarkerGL = () => {
  const markerRef = useRef<Marker>(null);
  const popup = useMemo(() => {
    return new maplibregl.Popup().setText("Hello world!");
  }, []);

  return (
    <Marker longitude={35.1252} latitude={31.883} popup={popup} ref={markerRef}>
      <img
        src={markerImage}
        alt="Marker Image"
        style={{
          backgroundSize: "cover",
          width: "50px",
          height: "50px",
          borderRadius: "50%",
          cursor: "pointer",
        }}
      />
    </Marker>
  );
};
export default MarkerGL;
4;
