// import {
//   Box,
//   // Button,
//   IconButton,
//   ImageList,
//   ImageListItem,
//   ImageListItemBar,
//   // Paper,
// } from "@mui/material";
// import InfoIcon from "@mui/icons-material/Info";
// interface CardProps {
//   showCard: boolean;
//   setShowCard: React.Dispatch<React.SetStateAction<boolean>>;
// }
// const Card: React.FC<CardProps> = ({ showCard }) => {
//   return (
//     <Box>
//       {showCard && (
//         <ImageList sx={{ width: 400 }}>
//           <ImageListItem>
//             <img
//               src="https://www.touristisrael.com/wp-content/uploads/2011/02/jerusalem-purple-1024x683.jpg"
//               alt="הכותל המערבי"
//               loading="lazy"
//             />
//             <ImageListItemBar
//               title="הכותל המערבי"
//               subtitle="הכותל המערבי"
//               actionIcon={
//                 <IconButton
//                   sx={{ color: "rgba(255, 255, 255, 0.54)" }}
//                   // aria-label={`info about ${itemData[0].title}`}
//                 >
//                   <InfoIcon />
//                 </IconButton>
//               }
//             />
//           </ImageListItem>
//         </ImageList>
//       )}
//     </Box>
//   );
// };
// export default Card;
