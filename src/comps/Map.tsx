import "maplibre-gl/dist/maplibre-gl.css";
import * as React from "react";
import Marker from "./Marker";
import Map, {
  FullscreenControl,
  GeolocateControl,
  NavigationControl,
  ScaleControl,
} from "react-map-gl/maplibre";

import { Box, Grid, Paper } from "@mui/material";
const MapGL: React.FC = () => {
  const [viewState, setViewState] = React.useState({
    longitude: 35.1252,
    latitude: 31.883,
    zoom: 7,
  });

  return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justifyContent="center"
      style={{ minHeight: "100vh" }}
    >
      <Box sx={{ display: "flex", alignItems: "center" }}>
        <Paper elevation={8} style={{ padding: 20 }}>
          <Map
            {...viewState}
            onMove={(evt) => {
              setViewState(evt.viewState);
            }}
            style={{ width: 600, height: 400 }}
            mapStyle="https://api.maptiler.com/maps/streets/style.json?key=xgokJDpau2XaVaHbcfWt"
            attributionControl={false}
          >
            <FullscreenControl style={{ backgroundColor: "GoldenRod" }} />
            <GeolocateControl style={{ backgroundColor: "SeaGreen" }} />
            <NavigationControl style={{ backgroundColor: "DodgerBlue" }} />
            <ScaleControl
              position="bottom-right"
              style={{ backgroundColor: "salmon" }}
            />
            <Marker />
          </Map>
        </Paper>
      </Box>
    </Grid>
  );
};
export default MapGL;
