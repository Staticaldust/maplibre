import { createTheme } from "@mui/material/styles";
const theme = createTheme({
  palette: {
    mode: "dark",
    background: {
      default: "#1D1D1D",
    },
  },
});
export default theme;
